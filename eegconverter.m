%//JSONLab bindings
%//Download and unzip JSONLab from https://github.com/fangq/jsonlab

addpath('~/git/github/jsonlab');

%//FFT setup
%//FFTSize set to 65536, but https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/fftSize

debug   = 0;
eegsave = 1;
grafix  = 0;
sss     = 16;
fftsize = 2^sss;
samples = [1, fftsize ];

%//Load File
file    = './wavs/donna.wav';
info    = audioinfo( file );
iters   = floor(info.TotalSamples / fftsize) + 1;
eeg     = zeros(iters, 8);
si      = 0;

while si < iters
  sis = (si*fftsize+1);
  sie = ((si+1)*fftsize);
  if (sie > info.TotalSamples)
    sie = info.TotalSamples;
  endif
  printf("iter %03d/%03d: read samples [%09d,%09d] from %s\n", si, iters, sis, sie, file);

  [y,Fs] = audioread( file, [ sis sie ]);
  df = Fs/fftsize;
  fAxis = 0:df:(Fs-df);

  Nsamps = length(y);
  t = (1/Fs)*(1:Nsamps);           %Prepare time data for plot

  win   = hamming(fftsize);
  y_fft = abs(  fft(y, fftsize)  /  sum(win)  );
  y_fft = mean(y_fft,2);
  f = Fs*(0:Nsamps/2-1)/Nsamps;    %Prepare freq data for plot

  telms = 0;
  mtx   = zeros(8, 2);
  c     = 0;
  p     = 1;
  i     = 1;
  s     = 0;
  e     = 1;

  while i <= length(y_fft)
   k = (i-1)/2;
   if (k == 2^s || k == 0)
     if (k > 0)
       e = 2^(s+1);
       s++;
     else
       e = 2^(s);
     endif
     elems = (e-k);
     telms += elems;
     c++;
     p = k;
     x = 0;
     r = c;
     if ( c > 8 )
       r = c - 8;
     endif
     ch_r = 0;
     ch_l = 0;
     while x < elems
       if ( k == 0 )
         ch_i = k*2+1;
       else
         ch_i = k*2+x*2+1;
       endif
       ch_r += ( 20 * log10(y_fft(ch_i)));
       ch_l += ( 20 * log10(y_fft(ch_i+1)));
       x++;
     endwhile
     mtx(r,1) = ( mtx(r,1) + ch_l/elems ) / ceil(c/8);
     mtx(r,2) = ( mtx(r,1) + ch_l/elems ) / ceil(c/8);
     if ( debug )
       printf("\t%02d (%02d, %d): start=%05d, end=%05d (elems=%05d)\n",c,x,ceil(c/8),k,e,elems);
     endif
   endif
   %printf("%d Hz: %d %d Db\n", (i-1)*df/2, 20 * log10(y_fft(i:i+1)) );
   i=i+2;
  endwhile
  if ( debug )
    printf("\ttotal elems %d (fftsize: 2^%d=%d)\n",telms,sss,fftsize);
  endif

  si++;
  eeg(si,1:8) = mtx(1:8,1);
endwhile

%//Save eeg matrix to CSV & JSON
if ( eegsave )
  csvwrite('./data.txt', eeg);
  savejson('eeg', eeg, './data.json');
endif

%//Plot eeg matrix
if ( grafix )
  figure;
  plot(eeg(1:iters,1:8));
  ylim([-80 -40])
  xlabel('iters');
  ylabel('power');
  title('eeg');
endif
